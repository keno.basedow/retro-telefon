#pragma once

#include <Arduino.h>
#include "SendBuffer.h"
#include "Number.h"

class Command
{
public:

  Command()
    : type(INVALID)
  {
  }

  Command(uint8_t a_type)
    : type(a_type)
  {
  }

  Command(uint8_t a_type, Number a_number)
    : type(a_type)
    , number(a_number)
  {
  }

  bool invalid()
  {
    return (type == INVALID);
  }

  void write(SendBuffer* sendBuffer)
  {
    sendBuffer->reset();
    sendBuffer->add(FRAMING);
    sendBuffer->add(type);
    if (type == NUMBER) {
      for (int i = 0; i < number.getlength(); i++)
        sendBuffer->add(number.getDigit(i));
    }
    sendBuffer->add(FRAMING);
  }

  Number getNumber()
  {
    return number;
  }

  uint8_t getType()
  {
    return type;
  }

  static Command extract(uint8_t* data, size_t length)
  {
    if (length < 3)
      return Command();
    if (data[0] != FRAMING)
      return Command();
    uint8_t type = data[1];
    Number number;
    if (type == NUMBER)
    {
      size_t i = 2;
      while (i < length && data[i] != FRAMING)
      {
        number.addDigit(data[i++]);
      }
    }
    return Command(type, number);
  }

  static const uint8_t INVALID = 0;
  static const uint8_t NUMBER = 1;
  static const uint8_t CORRECT = 2;
  static const uint8_t WRONG = 3;
  static const uint8_t HUNGUP = 4;

private:

  uint8_t type;
  Number number;

  static const uint8_t FRAMING = 0xFF;

};

#pragma once

#include <Arduino.h>

class SendBuffer
{
public:

  SendBuffer()
  {
  }

  void reset()
  {
    pos = 0;
  }

  inline void add(uint8_t byte)
  {
    if (pos >= MAX_SIZE)
    {
      pos = 0;
    }
    buffer[pos++] = byte;
  }

  inline int length()
  {
    return pos;
  }

  const uint8_t* data()
  {
    return buffer;
  }

  static const int MAX_SIZE = 1024;

private:

  uint8_t buffer[MAX_SIZE];
  int pos = 0;

};

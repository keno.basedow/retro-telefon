#pragma once

#include "Arduino.h"
#include "WiFi.h"
#include <AsyncUDP.h>
#include "SendBuffer.h"
#include "ReceiveBuffer.h"
#include "Command.h"

void redirectOnPacket(void* arg, AsyncUDPPacket packet);

class Network
{
public:

  Network(SendBuffer* a_sendBuffer, ReceiveBuffer* a_receiveBuffer)
    : sendBuffer(a_sendBuffer)
    , receiveBuffer(a_receiveBuffer)
  {
    // sender
    // 24:62:AB:F3:01:CC
    // receiver
    // 24:0A:C4:5F:7F:48
    sender = (WiFi.macAddress() == "24:62:AB:F3:01:CC");
  }

  inline bool isSender()
  {
    return sender;
  }

  void connect()
  {
    if (sender)
    {
      Serial.println("Sender");
      Serial.println("Connecting to to access point 'Telefon'");
      WiFi.begin("Telefon", "changeme");

      while (WiFi.status() != WL_CONNECTED)
      {
        delay(500);
        Serial.print('.');
      }
      Serial.println();
      Serial.println("Connected to access point");  

      Serial.print("IP is: ");
      Serial.println(WiFi.localIP());

      Serial.println("Starting UDP server on port 8001");
      if (!udpIn.listen(8001))
      {
        Serial.println("Error starting UDP server");
      }
      Serial.println("UDP server started");

      Serial.println("Connecting to UDP server 192.168.4.1:8000");
      if (!udpOut.connect(IPAddress(192,168,4,1), 8000))
      {
        Serial.println("Error connecting to UDP server");
      }
      Serial.println("UDP server connected");
    }
    else
    {
      Serial.println("Receiver");
      Serial.println("Setting up access point 'Telefon'");
      if (!WiFi.softAP("Telefon", "changeme"))
      {
        Serial.println("Error setting up access point");
      }

      Serial.print("IP is: ");
      Serial.println(WiFi.softAPIP());

      Serial.println("Starting UDP server on port 8000");
      if (!udpIn.listen(8000))
      {
        Serial.println("Error starting UDP server");
      }
      Serial.println("UDP server started");

      Serial.println("Connecting to UDP server 192.168.4.2:8001");
      if (!udpOut.connect(IPAddress(192,168,4,2), 8001))
      {
        Serial.println("Error connecting to UDP server");
      }
      Serial.println("UDP server connected");
    }
    
    udpIn.onPacket(redirectOnPacket, this);
  }

  bool isConnected()
  {
    if (sender)
      return (WiFi.status() == WL_CONNECTED) && udpIn.connected() && udpOut.connected();
    else
      return udpIn.connected() && udpOut.connected();
  }

  void send()
  {
    //Serial.print("sending data with size: ");
    //Serial.println(sendBuffer->length());

    udpOut.write(sendBuffer->data(), sendBuffer->length());
    sendBuffer->reset();
  }

  void sendCommand(Command command)
  {
    command.write(sendBuffer);
    send();
  }

  void reset()
  {
    sendBuffer->reset();
    receiveBuffer->reset();
    lastCommand = Command();
  }

  void processPacket(AsyncUDPPacket packet)
  {
    //Serial.print("received data with size: ");
    //Serial.println(packet.length());

    Command command = Command::extract(packet.data(), packet.length());
    
    if (command.invalid())
    {
      receiveBuffer->processPacket(packet);
      return;
    }

    lastCommand = command;
  }

  Command getLastComamnd()
  {
    return lastCommand;
  }

private:

  bool sender;
  AsyncUDP udpIn;
  AsyncUDP udpOut;
  SendBuffer* sendBuffer;
  ReceiveBuffer* receiveBuffer;
  Command lastCommand;

};

void redirectOnPacket(void* arg, AsyncUDPPacket packet)
{
  ((Network*)arg)->processPacket(packet);
}

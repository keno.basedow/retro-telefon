#pragma once

#include <Arduino.h>

class Microphone
{
public:

  Microphone()
  {
  }

  uint8_t getAudio()
  {
    value = analogRead(33);
    return map(value, 0, 2950, 0, 254);
  }

private:

  int value = 0;

  static const uint8_t PIN = 33;

};

#pragma once

#include <Arduino.h>
#include <AsyncUDP.h>

class ReceiveBuffer
{
public:

  ReceiveBuffer()
  {
  }

  void reset()
  {
    readPos = 0;
    writePos = 0;
  }

  void processPacket(AsyncUDPPacket packet)
  {
    if (length() > MAX_SIZE)
      Serial.println("overwriting data");
    
    packet.read(buffer + writePos, MAX_SIZE);
    
    if (writePos == 0)
      writePos = MAX_SIZE;
    else
      writePos = 0;
  }

  inline int length()
  {
    if (readPos <= writePos)
      return writePos - readPos;
    else
      return DOUBLE_SIZE - readPos + writePos;
  }

  inline uint8_t get()
  {
    if (readPos >= DOUBLE_SIZE)
      readPos = 0;
    if (length() > 0)
      return buffer[readPos++];
    else
      return 0;
  }

private:

  static const int MAX_SIZE = 1024;
  static const int DOUBLE_SIZE = MAX_SIZE * 2;
  uint8_t buffer[DOUBLE_SIZE];
  int writePos = 0;
  int readPos = 0;

};

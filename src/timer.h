#pragma once

float timerSec();
unsigned long timerMSec();

void timerInit();
void timerReset();
void timerEnable();
void timerDisable();
void timerRun();

void timerCallback();

#pragma once

#include <Arduino.h>

class Number
{
public:

  Number()
  {
  }

  Number(uint8_t* a_digits, size_t a_length)
  {
    length = a_length;
    for (size_t i = 0; i < length; i++)
      digits[i] = a_digits[i];
  }

  void addDigit(uint8_t digit)
  {
    if (length >= MAX_SIZE)
      return;

    digits[length++] = digit;
  }

  size_t getlength() const
  {
    return length;
  }

  uint8_t getDigit(size_t i) const
  {
    if (i >= length)
      return 0;

    return digits[i];
  }

  void print() const
  {
    for (int i = 0; i < length; i++)
      Serial.print(digits[i]);
  }

private:

  static const size_t MAX_SIZE = 20;

  uint8_t digits[MAX_SIZE];
  size_t length = 0;

};

inline bool operator==(const Number& lhs, const Number& rhs)
{
  if (lhs.getlength() != rhs.getlength())
    return false;
  
  for (size_t i = 0; i < lhs.getlength(); i++)
  {
    if (lhs.getDigit(i) != rhs.getDigit(i))
      return false;
  }
  return true;
}

inline bool operator!=(const Number& lhs, const Number& rhs)
{
  return !(lhs == rhs);
}

#include <Arduino.h>
#include <StateMachine.h>
#include <RotaryDial.h>

#include "timer.h"
#include "Cradle.h"
#include "Bell.h"
#include "Speaker.h"
#include "Microphone.h"
#include "Network.h"
#include "SendBuffer.h"
#include "ReceiveBuffer.h"
#include "Command.h"
#include "Number.h"

StateMachine machine = StateMachine();
Cradle cradle;
Bell bell;
Speaker speaker;
Microphone microphone;
SendBuffer sendBuffer;
ReceiveBuffer receiveBuffer;
Network network(&sendBuffer, &receiveBuffer);
Number number;

size_t numberLength;
Number telefonNumber;
Number telefonNumber2;
Number telefonNumber3;
int telefonNumberCount;

static const float NUMBER_TON_SECONDS = 0.100f;
//static const float NUMBER_FINISHED_SECONDS = 3.000f;
float lastNumberSeconds = 0.000f;
int lastNumber = 0;

// --------------------------------------------------
// hung up
// --------------------------------------------------
bool isHungUp()
{
  if (cradle.isHungUp() || false == network.isConnected())
  {
    Serial.println("is hung up");
    return true;
  }
  return false;
}

State* hungUp = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.println("hung up");
    network.reset();

    network.sendCommand(Command(Command::HUNGUP));
  }

  RotaryDial::readPulses();
  timerReset();
});

// --------------------------------------------------
// picked up
// --------------------------------------------------
bool isPickedUp()
{
  if (cradle.isPickedUp())
  {
    Serial.println("is picked up");
    return true;
  }
  return false;
}

State* pickedUp = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.println("picked up");
    network.reset();
  }

  speaker.playFreeTone();
});

// --------------------------------------------------
// got number
// --------------------------------------------------
bool hasGotNumber()
{
  if (bool(RotaryDial::available()))
  {
    Serial.println("has got number");
    return true;
  }
  return false;
}

State* gotNumber = machine.addState([]()
{
  if (machine.executeOnce)
  {
    lastNumber = RotaryDial::read();
    lastNumberSeconds = timerSec();
    Serial.print("got number: ");
    Serial.println(lastNumber);

    number.addDigit(lastNumber);
  }

  speaker.playNumberTone();
});

// --------------------------------------------------
// dialing
// --------------------------------------------------
bool isDialing()
{
  if ((timerSec() - lastNumberSeconds) > NUMBER_TON_SECONDS)
  {
    Serial.println("is dailing");
    return true;
  }
  return false;
}

State* dialing = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.println("dialing");
  }
});

// --------------------------------------------------
// send number
// --------------------------------------------------
bool isNumberFinished()
{
  //if ((timerSec() - lastNumberSeconds) > NUMBER_FINISHED_SECONDS)
  if (number.getlength() >= numberLength)
  {
    Serial.println("is finished number");
    return true;
  }
  return false;
}

State* sendNumber = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.print("sending number: ");
    number.print();
    Serial.println();
    network.sendCommand(Command(Command::NUMBER, number));
    number = Number();
  }

  speaker.playRingTone();
});

// --------------------------------------------------
// send number is wrong
// --------------------------------------------------
bool hasReceivedWrongNumber()
{
  if (network.getLastComamnd().getType() == Command::NUMBER)
  {
    Number receivedNumber = network.getLastComamnd().getNumber();
    if (receivedNumber != telefonNumber &&
        receivedNumber != telefonNumber2 &&
        receivedNumber != telefonNumber3)
    {
      Serial.println("received wrong number: ");
      receivedNumber.print();
      Serial.println();
      // Serial.println("expected number: ");
      // telefonNumber.print();
      // Serial.println();
      return true;
    }
  }
  return false;
}

State* sendNumberIsWrong = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.println("sending number is wrong");
    network.reset();

    network.sendCommand(Command(Command::WRONG));
  }
});

// --------------------------------------------------
// ringing
// --------------------------------------------------
bool hasReceivedCorrectNumber()
{
  if (network.getLastComamnd().getType() == Command::NUMBER)
  {
    Number receivedNumber = network.getLastComamnd().getNumber();
    if (receivedNumber == telefonNumber)
    {
      telefonNumberCount = 1;
      Serial.println("received correct number 1: ");
      receivedNumber.print();
      Serial.println();
      return true;
    }
    if (receivedNumber == telefonNumber2)
    {
      telefonNumberCount = 2;
      Serial.println("received correct number 2: ");
      receivedNumber.print();
      Serial.println();
      return true;
    }
    if (receivedNumber == telefonNumber3)
    {
      telefonNumberCount = 3;
      Serial.println("received correct number 3: ");
      receivedNumber.print();
      Serial.println();
      return true;
    }
  }
  return false;
}

State* ringing = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.println("ringing");
    network.reset();
  }
  if (telefonNumberCount == 1)
    bell.ring();
  else if (telefonNumberCount == 2)
    bell.ring2();
  else if (telefonNumberCount == 3)
    bell.ring3();
});

// --------------------------------------------------
// in call
// --------------------------------------------------
bool hasReceivedCorrect()
{
  if (network.getLastComamnd().getType() == Command::CORRECT)
  {
    Serial.println("has received correct");
    return true;
  }
  return false;
}

State* inCall = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.println("in call");
    network.reset();

    network.sendCommand(Command(Command::CORRECT));
  }

  sendBuffer.add(microphone.getAudio());
  if (sendBuffer.length() >= SendBuffer::MAX_SIZE)
    network.send();

  speaker.play(receiveBuffer.get());
});

// --------------------------------------------------
// occupied
// --------------------------------------------------
bool hasReceivedHungUp()
{
  if (network.getLastComamnd().getType() == Command::HUNGUP)
  {
    Serial.println("has received hung up");
    return true;
  }
  return false;
}

State* occupied = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.println("occupied");
    network.reset();
  }

  speaker.playOccupiedTone();
});

// --------------------------------------------------
// no connection
// --------------------------------------------------
bool hasReceivedWrong()
{
  if (network.getLastComamnd().getType() == Command::WRONG)
  {
    Serial.println("has received wrong");
    return true;
  }
  return false;
}

State* noConnection = machine.addState([]()
{
  if (machine.executeOnce)
  {
    Serial.println("no connection");
    network.reset();
  }

  speaker.playNoConnectionTone();
});

void setup()
{
  Serial.begin(115200);
  Serial.println();

  if (network.isSender())
  {
    numberLength = 1;

    uint8_t numberArray[] = {0,1,3,3,5,9,5,8,1};
    telefonNumber = Number(numberArray, 9);
    uint8_t numberArray2[] = {0,1,4,3,4,9,7,6,5};
    telefonNumber2 = Number(numberArray2, 9);
  }
  else
  {
    numberLength = 9;

    uint8_t numberArray[] = {1};
    telefonNumber = Number(numberArray, 1);
    uint8_t numberArray2[] = {2};
    telefonNumber2 = Number(numberArray2, 1);
    uint8_t numberArray3[] = {3};
    telefonNumber3 = Number(numberArray3, 1);
  }

  pickedUp->addTransition(isHungUp, hungUp);
  gotNumber->addTransition(isHungUp, hungUp);
  dialing->addTransition(isHungUp, hungUp);
  sendNumber->addTransition(isHungUp, hungUp);
  inCall->addTransition(isHungUp, hungUp);
  occupied->addTransition(isHungUp, hungUp);
  noConnection->addTransition(isHungUp, hungUp);

  hungUp->addTransition(isPickedUp, pickedUp);
  hungUp->addTransition(hasReceivedCorrectNumber, ringing);
  hungUp->addTransition(hasReceivedWrongNumber, sendNumberIsWrong);

  pickedUp->addTransition(hasGotNumber, gotNumber);
  dialing->addTransition(hasGotNumber, gotNumber);

  gotNumber->addTransition(isDialing, dialing);

  dialing->addTransition(isNumberFinished, sendNumber);

  sendNumber->addTransition(hasReceivedCorrect, inCall);
  sendNumber->addTransition(hasReceivedWrong, noConnection);

  sendNumberIsWrong->addTransition(isHungUp, hungUp);

  ringing->addTransition(isPickedUp, inCall);
  ringing->addTransition(hasReceivedHungUp, hungUp);

  inCall->addTransition(hasReceivedHungUp, occupied);

  RotaryDial::setup(14); // GPIO14
  timerInit();
}

void loop()
{
  if (network.isConnected())
  {
    timerRun();
  }
  else
  {
    Serial.println("Disconnected");
    timerDisable();
    network.connect();
    timerEnable();
  }
}

void timerCallback()
{
  machine.run();
}

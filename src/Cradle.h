#pragma once

#include <Arduino.h>

class Cradle
{
public:

  Cradle()
  {
    pinMode(PIN, INPUT_PULLUP);
  }

  void checkState()
  {
    if (pickedUp == false && digitalRead(PIN) == PICKEDUP && (millis() - stateMSec) > DEBOUNCE_MSEC)
    {
      stateMSec = millis();
      pickedUp = true;
    }
    if (pickedUp == true && digitalRead(PIN) == HUNGUP && (millis() - stateMSec) > DEBOUNCE_MSEC)
    {
      stateMSec = millis();
      pickedUp = false;
    }
  }

  bool isHungUp()
  {
    checkState();
    return (pickedUp == false);
  }

  bool isPickedUp()
  {
    checkState();
    return (pickedUp == true);
  }

private:

  bool pickedUp = false;
  unsigned long stateMSec = 0;
  
  static const uint8_t PIN = 34;
  static const int PICKEDUP = HIGH;
  static const int HUNGUP = LOW;
  static const int DEBOUNCE_MSEC = 50;

};

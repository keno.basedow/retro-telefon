#include <Arduino.h>
#include "timer.h"

volatile int interruptCounter = 0;
hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

static unsigned long counter = 0;

void IRAM_ATTR onTimer()
{
  portENTER_CRITICAL_ISR(&timerMux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}

void timerInit()
{
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 100, true);
}

void timerReset()
{
  counter = 0;
}

void timerEnable()
{
  if (timer)
    timerAlarmEnable(timer);
}

void timerDisable()
{
  if (timer)
    timerAlarmDisable(timer);
}

float timerSec()
{
  return (counter / 10000.0f);
}

unsigned long timerMSec()
{
  return (counter / 10UL);
}

void timerRun()
{
  if (interruptCounter > 0)
  {
    portENTER_CRITICAL(&timerMux);
    interruptCounter--;
    portEXIT_CRITICAL(&timerMux);

    counter++;

    timerCallback();
  }
}

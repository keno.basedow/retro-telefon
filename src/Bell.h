#pragma once

#include <Arduino.h>
#include "timer.h"

class Bell
{
private:

  static const uint8_t PIN_1 = 12;
  static const uint8_t PIN_2 = 13;

public:

  Bell()
  {
    pinMode(PIN_1, OUTPUT);
    pinMode(PIN_2, OUTPUT);
  }

  void ring()
  {
    if ((timerMSec() % 100UL) < 50UL)
    {
      digitalWrite(PIN_1, HIGH);
      digitalWrite(PIN_2, LOW);
    }
    else
    {
      digitalWrite(PIN_1, LOW);
      digitalWrite(PIN_2, HIGH);
    }
  }

  void ring2()
  {
    if ((timerMSec() % 500UL) < 100UL)
    {
      ring();
    }
  }

  void ring3()
  {
    if ((timerMSec() % 1000UL) < 100UL)
    {
      ring();
    }
  }

};

#pragma once

#include <Arduino.h>
#include "timer.h"

class Speaker
{
public:

  Speaker()
  {
  }

  void playFreeTone()
  {
    play(hzValue(440.0f) + hzValue(350.0f));
  }

  void playNumberTone()
  {
    play(hzValue(425.0f));
  }

  void playRingTone()
  {
    if ((timerMSec() % 5000UL) < 1000UL) {
      play(hzValue(425.0f));
    }
  }

  void playOccupiedTone()
  {
    if ((timerMSec() % 960UL) < 480UL) {
      play(hzValue(425.0f));
    }
  }

  void playNoConnectionTone()
  {
    unsigned long period = timerMSec() % 1990UL;
    if (period < 330UL)
      play(hzValue(950.0f));
    else if (period < 660UL)
      play(hzValue(1400.0f));
    else if (period < 990UL)
      play(hzValue(1800.0f));
  }

  void play(uint8_t audio)
  {
    dacWrite(PIN, audio);
  }

private:

  uint8_t hzValue(float hz)
  {
    return (sin(timerSec() * hz * 2.0f * PI) + 1.0f) * 16.0f;
  }

  static const uint8_t PIN = 26;

};

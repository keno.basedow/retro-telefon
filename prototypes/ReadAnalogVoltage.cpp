/*
  ReadAnalogVoltage

  Reads an analog input on pin 0, converts it to voltage, and prints the result to the Serial Monitor.
  Graphical representation is available using Serial Plotter (Tools > Serial Plotter menu).
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/ReadAnalogVoltage
*/

int sensorValue;
int count;
int minimum;
int average;
float factor;
int maximum;
unsigned long last;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(115200);
  //analogWriteFreq(500);
  count = 0;
  minimum = 4096;
  average = 0;
  factor = 0.01f;
  maximum = 0;
  last = micros();
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(34);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
//  float voltage = sensorValue * (3.3 / 4095.0);
  int audio = map(sensorValue, 0, 2950, 0, 255);
//  // print out the value you read:
//  if (sensorValue < minimum) minimum = sensorValue;
//  if (sensorValue > maximum) maximum = sensorValue;
//  average = factor * sensorValue + (1.0f - factor) * average;
  count++;
  if (count == 10000)
  {
//    Serial.println((micros() - last) / 10000);
//    Serial.print(1280);
//    Serial.print(" ");
//    Serial.print(minimum);
//    Serial.print(" ");
//    Serial.print(average);
//    Serial.print(" ");
//    Serial.print(maximum);
//    Serial.print(" ");
//    Serial.println(sensorValue);
//    Serial.print(" ");
    Serial.println(audio);
    count = 0;
    last = micros();
  }

  dacWrite(25, audio);
}

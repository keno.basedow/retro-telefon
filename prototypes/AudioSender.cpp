#include <WiFi.h>
#include <AsyncUDP.h>

AsyncUDP udp;
static const int PACKET_SIZE = 1024;
uint8_t buffer[PACKET_SIZE];
int pos = 0;

volatile int interruptCounter = 0;
hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

volatile int interruptCounter2 = 0;
hw_timer_t * timer2 = NULL;
portMUX_TYPE timerMux2 = portMUX_INITIALIZER_UNLOCKED;

int counter = 0;
int value = 0;
uint8_t audio = 0;

void IRAM_ATTR onTimer()
{
  portENTER_CRITICAL_ISR(&timerMux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}
 
void IRAM_ATTR onTimer2()
{
  portENTER_CRITICAL_ISR(&timerMux2);
  interruptCounter2++;
  portEXIT_CRITICAL_ISR(&timerMux2);
}

void setup()
{
  Serial.begin(115200);

  Serial.println("Connecting to Telefon");
  WiFi.begin("Telefon", "changeme");

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print('.');
  }

  Serial.println("Connection established!");  
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  Serial.println("Connecting to UDP server 192.168.4.1:8000");
  if (!udp.connect(IPAddress(192,168,4,1), 8000)) {
    Serial.println("Error connecting to UDP server");
  }
 
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 100000, true);
  timerAlarmEnable(timer);
 
  timer2 = timerBegin(1, 80, true);
  timerAttachInterrupt(timer2, &onTimer2, true);
  timerAlarmWrite(timer2, 45, true);
  timerAlarmEnable(timer2);
}

void loop()
{
  if (interruptCounter > 0)
  {
    portENTER_CRITICAL(&timerMux);
    interruptCounter--;
    portEXIT_CRITICAL(&timerMux);
 
//    Serial.print("Call per second: ");
//    Serial.println(counter);
    Serial.println(audio);
    counter = 0;
  }
  
  if (interruptCounter2 > 0)
  {
    portENTER_CRITICAL(&timerMux2);
    interruptCounter2--;
    portEXIT_CRITICAL(&timerMux2);
 
    counter++;

    value = analogRead(33);
    audio = map(value, 0, 2950, 0, 255);
    //dacWrite(26, audio);
    buffer[pos++] = audio;
    if (pos >= PACKET_SIZE)
    {
      udp.write(buffer, PACKET_SIZE);
      pos = 0;
    }
  }
  
  yield();
}

volatile int interruptCounter = 0;
hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

volatile int interruptCounter2 = 0;
hw_timer_t * timer2 = NULL;
portMUX_TYPE timerMux2 = portMUX_INITIALIZER_UNLOCKED;

int counter = 0;
uint8_t audio;

void IRAM_ATTR onTimer()
{
  portENTER_CRITICAL_ISR(&timerMux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}
 
void IRAM_ATTR onTimer2()
{
  portENTER_CRITICAL_ISR(&timerMux2);
  interruptCounter2++;
  portEXIT_CRITICAL_ISR(&timerMux2);
}
 
void setup()
{
  Serial.begin(115200);
 
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 1000000, true);
  timerAlarmEnable(timer);
 
  timer2 = timerBegin(1, 80, true);
  timerAttachInterrupt(timer2, &onTimer2, true);
  timerAlarmWrite(timer2, 45, true);
  timerAlarmEnable(timer2);
}
 
void loop()
{
  if (interruptCounter > 0)
  {
    portENTER_CRITICAL(&timerMux);
    interruptCounter--;
    portEXIT_CRITICAL(&timerMux);
 
    Serial.print("Call per secoond: ");
    Serial.println(counter);
    counter = 0;
  }
  
  if (interruptCounter2 > 0)
  {
    portENTER_CRITICAL(&timerMux2);
    interruptCounter2--;
    portEXIT_CRITICAL(&timerMux2);
 
    audio = map(analogRead(26), 0, 2950, 0, 255);
    counter++;
  }
}

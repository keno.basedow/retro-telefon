#include <WiFi.h>
#include <AsyncUDP.h>

AsyncUDP udp;
uint8_t buffer[2048];
int writePos = 0;
int readPos = 0;
int audio = 0;

volatile int interruptCounter = 0;
hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

volatile int interruptCounter2 = 0;
hw_timer_t * timer2 = NULL;
portMUX_TYPE timerMux2 = portMUX_INITIALIZER_UNLOCKED;

int counter = 0;
int dataSize = 0;

void IRAM_ATTR onTimer()
{
  portENTER_CRITICAL_ISR(&timerMux);
  interruptCounter++;
  portEXIT_CRITICAL_ISR(&timerMux);
}
 
void IRAM_ATTR onTimer2()
{
  portENTER_CRITICAL_ISR(&timerMux2);
  interruptCounter2++;
  portEXIT_CRITICAL_ISR(&timerMux2);
}

void onPacket(AsyncUDPPacket packet)
{
  Serial.print(audio);
  Serial.print(" ");
  Serial.println(dataSize);

  if (dataSize > 1024)
    Serial.println("overwriting data");
  
  packet.read(buffer + writePos, 1024);
  
  if (writePos == 0)
    writePos = 1024;
  else
    writePos = 0;
}

void setup()
{
  Serial.begin(115200);
  
  Serial.println("Setting up access point");
  WiFi.softAP("Telefon", "changeme");

  Serial.print("IP is: ");
  Serial.println(WiFi.softAPIP());

  Serial.println("Starting UDP server on port 8000");
  if (!udp.listen(8000)) {
    Serial.println("Error starting UDP server.");
  }
  Serial.println("UDP server started");
 
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 100000, true);
  timerAlarmEnable(timer);
 
  timer2 = timerBegin(1, 80, true);
  timerAttachInterrupt(timer2, &onTimer2, true);
  timerAlarmWrite(timer2, 45, true);
  timerAlarmEnable(timer2);

  udp.onPacket(onPacket);
}

void loop()
{
  if (interruptCounter > 0)
  {
    portENTER_CRITICAL(&timerMux);
    interruptCounter--;
    portEXIT_CRITICAL(&timerMux);

//    Serial.print("Call per secoond: ");
//    Serial.print(counter);
//    Serial.print(", size: ");
//    Serial.println(dataSize);
//    Serial.println(audio);
    counter = 0;
  }
  
  if (interruptCounter2 > 0)
  {
    portENTER_CRITICAL(&timerMux2);
    interruptCounter2--;
    portEXIT_CRITICAL(&timerMux2);
 
    counter++;

    if (readPos >= 2048)
      readPos = 0;
    if (readPos <= writePos)
      dataSize = writePos - readPos;
    else
      dataSize = 2048 - readPos + writePos;
    if (dataSize > 0)
    {
      audio = buffer[readPos++];
      dacWrite(26, audio);
    }
  }
}
